package com.atlassian.plugin.web.descriptors;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.loaders.LoaderUtils;
import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.web.baseconditions.AbstractConditionElementParser;
import com.atlassian.plugin.web.conditions.AndCompositeCondition;
import com.atlassian.plugin.web.baseconditions.CompositeCondition;
import com.atlassian.plugin.web.conditions.ConditionLoadingException;
import com.atlassian.plugin.web.conditions.InvertedCondition;
import com.atlassian.plugin.web.conditions.OrCompositeCondition;

import org.dom4j.Element;

import java.util.List;

/**
 * Implementation of {@link AbstractConditionElementParser} for
 * {@link Condition}
 * Its functionality is used by both
 * {@link com.atlassian.plugin.web.descriptors.AbstractWebFragmentModuleDescriptor}
 * and
 * {@link com.atlassian.plugin.web.descriptors.DefaultWebPanelModuleDescriptor}.
 *
 * @since 2.5.0
 */
public class ConditionElementParser extends AbstractConditionElementParser<Condition>
{
    /**
     * @deprecated use AbstractConditionElementParser.CompositeType
     */
    @Deprecated
    public static class CompositeType extends AbstractConditionElementParser.CompositeType {}

    /**
     * Creates a condition. Only temporary until conditions for web fragments can be converted to use {@link HostContainer}
     */
    public static interface ConditionFactory
    {
        Condition create(String className, Plugin plugin) throws ConditionLoadingException;
    }

    private final ConditionFactory conditionFactory;

    public ConditionElementParser(ConditionFactory conditionFactory)
    {
        this.conditionFactory = conditionFactory;
    }

    /**
     * Only here to retain binary compatibility. See {@link AbstractConditionElementParser#makeConditions(com.atlassian.plugin.Plugin, org.dom4j.Element, int)}
     */
    public Condition makeConditions(final Plugin plugin, final Element element, final int type) throws PluginParseException
    {
        return super.makeConditions(plugin, element, type);
    }

    /**
     * Only here to retain binary compatibility. See {@link AbstractConditionElementParser#makeConditions(com.atlassian.plugin.Plugin, java.util.List, int)}
     */
    public Condition makeConditions(final Plugin plugin, final List<Element> elements, final int type) throws PluginParseException
    {
        return super.makeConditions(plugin, elements, type);
    }

    /**
     * Only here to retain binary compatibility. See {@link AbstractConditionElementParser#makeCondition(com.atlassian.plugin.Plugin, org.dom4j.Element)}
     */
    public Condition makeCondition(final Plugin plugin, final Element element) throws PluginParseException
    {
        return super.makeCondition(plugin, element);
    }

    @Override
    protected Condition makeConditionImplementation(final Plugin plugin, final Element element) throws PluginParseException
    {
        try
        {
            String conditionClassName = element.attributeValue("class");
            if (conditionClassName == null)
            {
                throw new PluginParseException("Condition element must specify a class attribute");
            }
            final Condition condition = conditionFactory.create(conditionClassName, plugin);
            condition.init(LoaderUtils.getParams(element));

            return condition;
        } catch (final ClassCastException e)
        {
            throw new PluginParseException("Configured condition class does not implement the Condition interface", e);
        } catch (final ConditionLoadingException cle)
        {
            throw new PluginParseException("Unable to load the module's display conditions: " + cle.getMessage(), cle);
        }
    }

    @Override
    protected Condition invert(Condition condition)
    {
        return new InvertedCondition(condition);
    }

    @Override
    protected CompositeCondition<Condition> createAndCompositeCondition()
    {
        return new AndCompositeCondition();
    }

    @Override
    protected CompositeCondition<Condition> createOrCompositeCondition()
    {
        return new OrCompositeCondition();
    }
}