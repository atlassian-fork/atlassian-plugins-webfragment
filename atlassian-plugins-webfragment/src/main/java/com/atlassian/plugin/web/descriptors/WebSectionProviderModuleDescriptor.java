package com.atlassian.plugin.web.descriptors;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.util.validation.ValidationPattern;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.api.provider.WebSectionProvider;
import com.google.common.base.Supplier;
import org.dom4j.Element;

import javax.annotation.Nonnull;

import static com.atlassian.plugin.util.validation.ValidationPattern.test;
import static com.google.common.base.Suppliers.memoize;

public class WebSectionProviderModuleDescriptor extends AbstractModuleDescriptor<WebSectionProvider>
{
    private final WebInterfaceManager webInterfaceManager;
    private Supplier<WebSectionProvider> sectionSupplier;
    private String location;

    public WebSectionProviderModuleDescriptor(final ModuleFactory moduleFactory, final WebInterfaceManager webInterfaceManager)
    {
        super(moduleFactory);
        this.webInterfaceManager = webInterfaceManager;
    }

    @Override
    protected void provideValidationRules(ValidationPattern pattern)
    {
        super.provideValidationRules(pattern);
        pattern.rule(test("@class").withError("The web section provider class is required"));
        pattern.rule(test("@location").withError("Must provide a location that sections should be added to."));
    }

    @Override
    public void init(@Nonnull final Plugin plugin, @Nonnull final Element element) throws PluginParseException
    {
        super.init(plugin, element);
        final WebSectionProviderModuleDescriptor that = this;
        sectionSupplier = memoize(new Supplier<WebSectionProvider>()
        {
            @Override
            public WebSectionProvider get()
            {
                return moduleFactory.createModule(moduleClassName, that);
            }
        });
        location = element.attributeValue("location");
    }

    public String getLocation()
    {
        return location;
    }

    @Override
    public WebSectionProvider getModule()
    {
        return sectionSupplier.get();
    }

    @Override
    public void enabled()
    {
        super.enabled();
        webInterfaceManager.refresh();
    }

    @Override
    public void disabled()
    {
        super.disabled();
        webInterfaceManager.refresh();
    }
}
