# Atlassian Webfragments

## Description

A framework to allow Atlassian plugins to insert links and snippets of HTML into web applications.

## Atlassian Developer?

### Committing Guidelines

Please see [The Platform Rules of Engagement (go/proe)](http://go.atlassian.com/proe) for committing to this module.

### Builds

The Bamboo builds for this project are on [EcoBAC](https://ecosystem-bamboo.internal.atlassian.com/browse/PLUGFRAG).

### Historical Versions

This library was previously shipped as part of the [plugins](https://bitbucket.org/atlassian/atlassian-plugins)
framework, but was split into its own project on November 22nd, 2012.

## External User?

### Issues

Please raise any issues you find with this module in [JIRA](https://ecosystem.atlassian.net/browse/PLUGWEB).

### Documentation

- [JIRA Web Fragments](https://developer.atlassian.com/display/JIRADEV/Web+Fragments)
- [Confluence Web Fragments](https://developer.atlassian.com/display/CONFDEV/Web+UI+Modules)
- [General Documentation](https://developer.atlassian.com/display/DOCS/Web+Fragment+Module)
