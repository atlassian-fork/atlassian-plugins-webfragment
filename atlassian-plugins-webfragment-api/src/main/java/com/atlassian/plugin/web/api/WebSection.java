package com.atlassian.plugin.web.api;

import com.atlassian.annotations.PublicApi;

import javax.annotation.Nonnull;

/**
 * Represents a logical grouping of various other {@link com.atlassian.plugin.web.api.WebSection}s or {@link
 * WebItem}s in a specific location.
 *
 * @since v3.0.2
 */
@PublicApi
public interface WebSection extends WebFragment
{
    /**
     * @return the location a webSection belongs to
     */
    @Nonnull
    String getLocation();
}
