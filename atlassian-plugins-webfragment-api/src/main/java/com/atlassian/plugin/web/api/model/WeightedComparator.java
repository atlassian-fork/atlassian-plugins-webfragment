package com.atlassian.plugin.web.api.model;

import com.atlassian.plugin.web.api.WebFragment;

import java.util.Comparator;

/**
 * Comparator that can be used to sort weighted {@link com.atlassian.plugin.web.api.WebFragment}s
 *
 * @since v3.0.2
 */
public class WeightedComparator implements Comparator<WebFragment>
{
    public static final WeightedComparator WEIGHTED_FRAGMENT_COMPARATOR = new WeightedComparator();

    @Override
    public int compare(final WebFragment w1, final WebFragment w2)
    {
        if (w1.getWeight() < w2.getWeight())
        { return -1; }
        else if (w1.getWeight() > w2.getWeight())
        { return 1; }
        else
        { return 0; }
    }
}
