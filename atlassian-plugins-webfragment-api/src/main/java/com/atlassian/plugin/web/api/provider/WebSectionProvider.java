package com.atlassian.plugin.web.api.provider;

import com.atlassian.plugin.web.api.WebSection;

import java.util.Map;

/**
 * Allows plugins to provide dynamic web-sections for a location and given context. This is useful when sections should
 * be inserted in the UI based on data.
 *
 * @since v3.0.2
 */
public interface WebSectionProvider
{
    /**
     * Returns a list of web-sections for a given context. May be null.
     *
     * @return a list of web-sections for a given context. May be null.
     */
    Iterable<WebSection> getSections(final Map<String, Object> context);
}
