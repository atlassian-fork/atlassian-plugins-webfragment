package com.atlassian.plugin.web.baseconditions;

/**
 * Interface for composite conditions
 * @since v3.0
 */
public interface CompositeCondition<T extends BaseCondition> extends BaseCondition
{
    public void addCondition(T condition);
}
